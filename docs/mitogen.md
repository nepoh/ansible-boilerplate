# Mitogen

To use Mitogen, uncomment the following two lines in `ansible.cfg`:
```
#strategy_plugins = ...
#strategy = ...
```
