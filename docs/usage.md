# Usage
We installed Ansible in a virtual environment, so remember to activate it:
```shell
source ./venv/bin/activate
```

## Run a playbook
Run a playbook:
```shell
ansible-playbook playbooks/<playbook>.yml
```

Some useful options:
- `--ask-vault-pass` Ask for the vault password
  (if you have not stored it in the system keyring)
- `--check` Execute a playbook in _check mode_
  (will not make any changes on the remote hosts)
- `--diff` Show the diff of files that would change
- `--limit` Limit the affected hosts
  (e.g. `--limit=development` to play only on hosts in the `development` group)

## Run a single role
```shell
ansible <host> -m include_role -a name=<role_name>
```

## Usefull commands
Check if all hosts are reachable:
```shell
ansible all -m ping
```

Gather some information about hosts:
```shell
ansible all -m setup
```

Execute a `<command>` as `<user>` on all `<hosts>`
with sudo privileges (ask for the password first):
```shell
ansible <hosts> -m shell -a '<command>' -u <user> -b -K
```

## Helper scripts
There are some useful helper scripts located in the `./bin` subdirectory.

Run a command on multiple hosts (without or with root privileges):
```shell
bin/run all 'ls -la ~'
bin/run-sudo all 'systemctl status'
```

Copy a file to multiple hosts (without or with root privileges):
```shell
bin/copy all ./local/file.txt '~/remote/target/'
bin/copy-sudo all ./hosts /etc/hosts
```
