# Ansible Vault
[Ansible Vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html)
can be used to store passwords and other secrets within this repository.

You have to provide the vault password on every Ansible run
by using the `--ask-vault-password` switch:
```shell
ansible-playbook playbooks/my_playbook.yml --ask-vault-password
```

## Using the system keyring
The vault password can be stored in the system keyring
(any keyring supported by the [keyring](https://pypi.org/project/keyring/)
Python package can be used).
Use the helper script `bin/ansible-keyring` to set the password:
```shell
bin/ansible-keyring set
```

The script `bin/ansible-vault-password` reads the password from the keyring
and is referenced in `ansible.cfg` in order to automatically make it available
to Ansible:
```shell
vault_password_file = bin/ansible-vault-password
```

## Encrypting
```shell
ansible-vault encrypt_string
```
