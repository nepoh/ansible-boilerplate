###
# Settings
###

# Execute the `help` target by default
.DEFAULT_GOAL := help


###
# Targets
###

.PHONY: setup
setup: venv/bin/activate galaxy/.install	## Set up and install dependencies

.PHONY: update
update: setup                             	## Update dependencies
	@venv/bin/pip install --quiet --upgrade pip
	@venv/bin/pip install --quiet --upgrade --requirement python_requirements.txt
	@venv/bin/pip freeze > python_requirements.freeze
	@venv/bin/ansible-galaxy collection install --upgrade --force --requirements-file ansible_requirements.yml
	@venv/bin/ansible-galaxy role install --force --role-file ansible_requirements.yml

.PHONY: test
test: setup    								## Run tests
	@bin/test

.PHONY: lint
lint: setup    								## Run linter
	@bin/lint

.PHONY: help
help:                                      	## Show this help message
	@echo "Available targets:"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


###
# "Internal" targets
###

ansible.cfg: ansible.cfg-dist
	@test -e ansible.cfg || cp ansible.cfg-dist ansible.cfg

python_requirements.freeze: python_requirements.txt
	@cp python_requirements.txt python_requirements.freeze

venv/bin/activate: python_requirements.freeze
	@python3 -m venv venv/
	@venv/bin/pip install --quiet --upgrade pip
	@venv/bin/pip install --quiet --requirement python_requirements.freeze
	@venv/bin/pip freeze > python_requirements.freeze
	@cd venv/lib && test -e python || ln -s python*.* python

galaxy/.install: ansible.cfg ansible_requirements.yml
	@venv/bin/ansible-galaxy collection install --requirements-file ansible_requirements.yml
	@venv/bin/ansible-galaxy role install --role-file ansible_requirements.yml
	@touch galaxy/.install
