ansible
# the latest release of mitogen (0.3.4) supports ansible-core 2.13.x at max
ansible-core<2.14

ansible-lint
yamllint

# required by community.general.json_query filter
jmespath

keyring

# Mitogen 0.3 introduced breaking changes and 0.4 might as well
mitogen>=0.3.3,<0.4

molecule
molecule-plugins[docker]

# required by ansible.utils.ipaddr filter
netaddr

pytest
pytest-molecule
pytest-xdist
