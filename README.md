# Ansible project boilerplate

## Local installation
Python must be installed on the local machine:
```shell
apt install python3 python3-pip python3-venv
```

Then create a virtual environment and install Python and Ansible requirements:
```shell
make setup
source venv/bin/activate
```

Finally, adjust the newly created configuration file `ansible.cfg` to your needs.


### Add to existing project
```shell
git remote add boilerplate git@codeberg.org:nepoh/ansible-boilerplate.git
git fetch boilerplate
git checkout main
git merge --allow-unrelated-histories boilerplate/v3
```
Resolve potential merge conflicts and proceed with the setup:
```shell
make setup
source venv/bin/activate
```

## Ansible Vault
[Ansible Vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html)
can be used to store passwords and secrets within this repository.

For convenience, the vault password can be stored in the system keyring.
For details see [docs/vault.md](docs/vault.md).

## Inventory
All managed hosts and their affiliations to host groups
are configured in the inventory file `./inventory/hosts.yml`.
Specific configuration for individual hosts and host groups
can be added in `./inventory/host_vars/` and `./inventory/group_vars/`.

## Playbooks and roles
Playbooks can be found in `./playbooks/`, roles can be found in `./roles/`.

## Usage
For some basic information about using these playbooks,
see [docs/usage.md](docs/usage.md).

## Development

### Run syntax check
To run syntax checks provided by `ansible-lint` and `yamllint`, execute:
```shell
make lint
```

### Run tests
Some roles implement tests. To run them, execute
```shell
make test
```
[PyTest](https://docs.pytest.org/) collects all available tests
(with the help of the [pytest-molecule](https://pypi.org/project/pytest-molecule/) plugin)
and runs them with [Molecule](https://github.com/ansible-community/molecule) against Docker containers.

To run only tests for a specific, set the `ROLE` variable.
E.g. to only run tests for `role/ssl`, execute:
```shell
make test ROLE=ssl
```
