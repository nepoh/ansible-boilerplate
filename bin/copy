#!/usr/bin/env bash

set -o nounset
set -o pipefail

root_dir="$(dirname "$(dirname "$(readlink -f "$0")")")"

if [ $# -lt 3 ]; then
    echo "Copy a file to multiple hosts." >&2
    echo "Usage: $0 <hosts> <src> <dest>" >&2
    echo "" >&2
    echo "Positional arguments:" >&2
    echo "  <hosts>   Ansible host pattern" >&2
    echo "  <src>     Local source path" >&2
    echo "  <dest>    Remote destination path" >&2
    exit 1
fi

source "${root_dir}/venv/bin/activate"

ansible "$1" --module-name ansible.builtin.copy --args "src=${2} dest=${3}"
