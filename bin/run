#!/usr/bin/env bash

set -o nounset
set -o pipefail

root_dir="$(dirname "$(dirname "$(readlink -f "$0")")")"

positional=()
optional=()

while [ $# -gt 0 ]
do
    case $1 in
        --) positional+=("${@:2}"); break;;
        -*) optional+=("$1");       shift;;
        *)  positional+=("$1");     shift;;
    esac
done

if [[ ${#positional[@]} -lt 2 ]]; then
    echo "Usage: $0 [options] [--] pattern command [args...]" >&2
    echo "" >&2
    echo "Run a command on multiple hosts." >&2
    echo "" >&2
    echo "Positional arguments:" >&2
    echo "  pattern     Ansible host pattern" >&2
    echo "  command     Command with optional arguments" >&2
    echo "" >&2
    echo "Optional arguments:" >&2
    echo "  Any options will be passed through to the ansible command" >&2
    echo "  Make sure to pass options as single arguments," >&2
    echo "  e.g. '--user=foo' instead of '--user foo'" >&2
    echo "" >&2
    echo "Examples:" >&2
    echo "  $0 all uname" >&2
    echo "  $0 -- all ls -la" >&2
    echo "  $0 all 'cd /etc && cat fstab | grep nfs'" >&2
    echo "  $0 --ask-pass all uname" >&2
    echo "  " >&2

    exit 1
fi

source "${root_dir}/venv/bin/activate"

ansible "${positional[0]}" --module-name raw --args "/bin/bash -c '${positional[*]:1}'" "${optional[@]}"
